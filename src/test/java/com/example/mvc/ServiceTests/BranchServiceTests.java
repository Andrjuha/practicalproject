package com.example.mvc.ServiceTests;

import com.example.mvc.entities.Branch;
import com.example.mvc.repositories.BranchRepository;
import com.example.mvc.services.BranchService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BranchServiceTests {

    @Mock
    BranchRepository branchRepository;

    @InjectMocks
    BranchService branchService;

    @Test
    public void when_save_client_it_should_return_client() {
        Branch branch = new Branch();
        branch.setId(3L);


        Branch branchResponse = new Branch();
        branchResponse.setId(3L);


        when(branchRepository.save(branch)).thenReturn(branchResponse);

        Branch created = branchService.save(branch);

        assertTrue(created.getId().equals(branch.getId()));

    }
}
