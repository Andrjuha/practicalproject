package com.example.mvc.ServiceTests;

import com.example.mvc.entities.Booking;
import com.example.mvc.repositories.BookingRepository;
import com.example.mvc.services.BookingService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class BookingServiceTests {

    @Mock
    BookingRepository bookingRepository;

    @InjectMocks
    BookingService bookingService;

    @Test
    public void testBookingSave() {

        Booking bookingRequest = new Booking();
        bookingRequest.setStatus("test");
        bookingRequest.setId(3L);


        Booking bookingResponse = new Booking();
        bookingResponse.setStatus("test");
        bookingResponse.setId(3L);

        when(bookingRepository.save(bookingRequest)).thenReturn(bookingResponse);

        Booking created = bookingService.save(bookingRequest);

        assertTrue(created.getStatus().equals(bookingRequest.getStatus()));
        assertNotNull(created.getId());

    }

    @Test
    public void testFindAll() {
        List<Booking> bookings = List.of(new Booking(), new Booking(), new Booking());

        when(bookingRepository.findAll()).thenReturn(bookings);

        List<Booking> created = bookingService.findAll();

        assertNotNull(created);
        assertTrue(!created.isEmpty());
        assertTrue(created.size() == 3);

    }




}
