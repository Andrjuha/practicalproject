package com.example.mvc.services;

import com.example.mvc.entities.Booking;
import com.example.mvc.repositories.BookingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class BookingService {

    @Autowired
    private BookingRepository bookingRepository;

    public List<Booking> findAll() {
        List<Booking> bookings = bookingRepository.findAll();
        return bookings;
    }

    public Booking findById(Long id) {
        Booking booking = bookingRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid booking Id:" + id));
        return booking;
    }

    public Booking save(Booking id) {
        Booking bookings = bookingRepository.save(id);
        return bookings;
    }

    public void deleteById(Long id) {
        bookingRepository.deleteById(id);
    }

    public void delete(Booking b) {
        bookingRepository.delete(b);
    }
}
