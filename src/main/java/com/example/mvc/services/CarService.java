package com.example.mvc.services;

import com.example.mvc.entities.Car;
import com.example.mvc.repositories.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CarService {

    @Autowired
    private CarRepository carRepository;

    public List<Car> findAll() {
        List<Car> cars = carRepository.findAll();
        return cars;
    }

    public Car findById(Long id) {
        Car cars = carRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid car Id:" + id));
        return cars;
    }

    public Car save(Car id) {
        Car newCar = carRepository.save(id);
        return newCar;
    }

    public void delete(Car id) {
        carRepository.delete(id);
    }

    public void deleteById(Long id) {
        carRepository.deleteById(id);
    }

}
