package com.example.mvc.services;

import com.example.mvc.entities.Return;
import com.example.mvc.repositories.ReturnRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ReturnService {

    @Autowired
    private ReturnRepository returnRepository;

    public Return save(Return id) {
        Return aReturn = returnRepository.save(id);
        return aReturn;
    }

    public List<Return> findAll() {
        return returnRepository.findAll();
    }

    public Return findById(Long id) {
        return returnRepository.findById(id).get();
    }

    public void delete(Return obj) {
        returnRepository.delete(obj);
    }

    public void deleteById(Long id) {
        returnRepository.deleteById(id);
    }

}
