package com.example.mvc.services;

import com.example.mvc.entities.RentalOffice;
import com.example.mvc.repositories.RentalOfficeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class RentalOfficeService {

    @Autowired
    private RentalOfficeRepository rentalOfficeRepository;

    public List<RentalOffice> findAll(){
        List<RentalOffice> offices =  rentalOfficeRepository.findAll();
                return offices;
    }

    public RentalOffice findById(Long id){
        Optional<RentalOffice> office = rentalOfficeRepository.findById(id);
        return office.get();
    }

    public RentalOffice save(RentalOffice id) {
        RentalOffice newOffice = rentalOfficeRepository.save(id);
        return newOffice;
    }

}
