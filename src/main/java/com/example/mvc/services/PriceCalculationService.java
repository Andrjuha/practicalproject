package com.example.mvc.services;

import com.example.mvc.entities.Booking;
import org.springframework.stereotype.Service;


import javax.transaction.Transactional;
import java.math.BigDecimal;

@Service
@Transactional
public class PriceCalculationService {

    public Booking createBooking(Booking b, BigDecimal price) {
        return setBookingPrice(b, price);
    }

    public Booking addExtraPrice(Booking b, BigDecimal price) {
        return setBookingPrice(b, price);
    }

    public Booking cancelBooking(Booking booking) {
        return setBookingPrice(booking, new BigDecimal(0.00));
    }

    public Booking lateCancelBooking(Booking booking) {
        return setBookingPrice(booking, booking.getRevenue().getTotalSum().multiply(new BigDecimal(0.2)));
    }

    public Booking setBookingPrice(Booking b, BigDecimal extra) {
        BigDecimal finalPrice = b.getRevenue().getTotalSum().add(extra);
        b.getRevenue().setTotalSum(finalPrice);
        return b;
    }


}
