package com.example.mvc.entities;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Data
public class Revenue {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;

    @Column(name = "total_sum", nullable= false, precision=10, scale=2)
    private BigDecimal totalSum; // 1300.00



}
