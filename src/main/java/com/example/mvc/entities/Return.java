package com.example.mvc.entities;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "returns")
@Data
public class Return {

   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Id
   private Long id;

   @OneToOne
   private Employee employee;

   @NotNull(message = "Date is mandatory")
   private Date returnDate;

   @NotBlank
   @NotNull(message = "Additional payment is mandatory")
   private String additionalPayment;

   @OneToMany(mappedBy = "aReturn", cascade = CascadeType.ALL)
   private List<Comment> comments;

}
