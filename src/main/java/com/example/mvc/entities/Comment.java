package com.example.mvc.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Data
@NoArgsConstructor
public class Comment {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;

    @NotBlank(message = "Author is mandatory")
    private String author;

    @NotBlank(message = "Text is mandatory")
    private String commentText;

    @ManyToOne(fetch = FetchType.LAZY)
    private Return aReturn;

    @ManyToOne(fetch = FetchType.LAZY)
    private Rent rent;


}
