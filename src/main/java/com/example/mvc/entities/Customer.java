package com.example.mvc.entities;


import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class Customer {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;

    @NotBlank(message = "Name is mandatory")
    private String firstName;

    @NotBlank(message = "Surname is mandatory")
    private String lastName;

    @Email
    @NotBlank(message = "Email is mandatory")
    @NotNull
    private String email;

    @NotBlank(message = "Address is mandatory")
    @OneToMany(mappedBy = "customer")
    private List<Address> address;

    @OneToMany(mappedBy = "client")
    private List<Booking> booking;

}
