package com.example.mvc.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.List;

//CR-203, wasn't completed, CR-205 failed as a dependency

@Entity
@Data
@NoArgsConstructor
public class Branch {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;

    @NotBlank(message = "Address is mandatory")
    private String address;

    @OneToMany(mappedBy = "branch", cascade = CascadeType.ALL)
    private List<Employee> employeeList;

    @OneToMany(mappedBy = "branch", cascade = CascadeType.ALL)
    private List<Car> availableCars;



}
