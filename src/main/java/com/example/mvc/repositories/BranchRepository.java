package com.example.mvc.repositories;

import com.example.mvc.entities.Branch;
import com.example.mvc.entities.RentalOffice;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BranchRepository extends CrudRepository<Branch, Long> {

    List<Branch> findAll();

    Optional<Branch> findById(Long id);

    void deleteById(Long id);
}
