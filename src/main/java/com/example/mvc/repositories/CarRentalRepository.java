package com.example.mvc.repositories;

import com.example.mvc.entities.Rent;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarRentalRepository extends CrudRepository<Rent, Long> {

    List<Rent> findAll();

    void deleteById(Long id);

    void delete(Rent rent);

}
