package com.example.mvc.repositories;

import com.example.mvc.entities.RentalOffice;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RentalOfficeRepository extends CrudRepository<RentalOffice, Long> {

    List<RentalOffice> findAll();

    Optional<RentalOffice> findById(Long id);

}
