package com.example.mvc.controllers;

import com.example.mvc.entities.Message;
import com.example.mvc.entities.Return;
import com.example.mvc.services.ReturnService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/return")
@RequiredArgsConstructor
public class ReturnController {

    private final ReturnService returnService;

    @GetMapping("/showAll")
    public ResponseEntity<List<Return>> showAll(@Valid @RequestBody Return rentalOffice) {
        return ResponseEntity.ok(returnService.findAll());
    }

    @GetMapping("/show/{id}")
    public ResponseEntity<Return> showById(@PathVariable Long id) {
        return ResponseEntity.ok(returnService.findById(id));
    }

    @PostMapping("/add")
    public ResponseEntity<Return> addReturn(@Valid @RequestBody Return returnManagement) {
        return ResponseEntity.ok(returnService.save(returnManagement));
    }

    @DeleteMapping("/delete")
    public ResponseEntity<Message> delete(Return id) {
        returnService.delete(id);
        return ResponseEntity.ok(new Message("Return deleted"));
    }

    @DeleteMapping("/delete/id")
    public ResponseEntity<Message> deleteById(Long id) {
        returnService.deleteById(id);
        return ResponseEntity.ok(new Message("Return deleted"));
    }
}
