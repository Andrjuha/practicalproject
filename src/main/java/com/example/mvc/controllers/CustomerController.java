package com.example.mvc.controllers;


import com.example.mvc.entities.Customer;
import com.example.mvc.entities.Message;
import com.example.mvc.services.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/customer")
@RequiredArgsConstructor
public class CustomerController {

    private final CustomerService customerService;

    @GetMapping("/showAll")
    public ResponseEntity<List<Customer>> fetchCustomers() {
        return ResponseEntity.ok(customerService.findAll());
    }

    @PostMapping("/save") //adds and/or updates
    public ResponseEntity<Customer> addCustomer(@Valid @RequestBody Customer customer) {
        return ResponseEntity.ok(customerService.save(customer));
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Message> deleteCustomerById(@PathVariable("id") Long id) {
        customerService.deleteById(id);
        return ResponseEntity.ok(new Message("Record deleted successfully"));
    }

    @DeleteMapping("/delete")
    public ResponseEntity<Message> deleteCustomer(@RequestBody Customer id) {
        customerService.delete(id);
        return ResponseEntity.ok(new Message("Record deleted successfully"));
    }
}
