package com.example.mvc.controllers;

import com.example.mvc.entities.Booking;
import com.example.mvc.entities.PriceRequest;
import com.example.mvc.services.PriceCalculationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
@RequiredArgsConstructor
@RequestMapping("/price")
public class PriceCalculationController {

    private final PriceCalculationService priceService;

    @PostMapping("/create")
    public ResponseEntity<Booking> createBooking(@RequestBody PriceRequest request) {
        Booking b = priceService.createBooking(request.getBooking(), new BigDecimal(request.getExtraPayment()));
        return ResponseEntity.ok(b);
    }

    @PostMapping("/cancel")
    public ResponseEntity<Booking> cancelBooking(@RequestBody Booking booking) {
        Booking b = priceService.cancelBooking(booking);
        return ResponseEntity.ok(b);
    }

    @PostMapping("/cancel-late")
    public ResponseEntity<Booking> lateCancel(@RequestBody Booking booking) {
        Booking b = priceService.lateCancelBooking(booking);
        return ResponseEntity.ok(b);
    }

    @PostMapping("/add-extra")
    public ResponseEntity<Booking> addExtra(@RequestBody PriceRequest request) {
        Booking b = priceService.addExtraPrice(request.getBooking(), new BigDecimal(request.getExtraPayment()));
        return ResponseEntity.ok(b);
    }
}
