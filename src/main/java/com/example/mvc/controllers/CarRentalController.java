package com.example.mvc.controllers;

import com.example.mvc.entities.*;
import com.example.mvc.services.CarRentalService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/carRental")
public class CarRentalController {

    private final CarRentalService carRentalService;

    @GetMapping("/showAll")
    public ResponseEntity<List<Rent>> showAll(@Valid @RequestBody Rent car) {
        return ResponseEntity.ok(carRentalService.findAll());
    }

    @PostMapping("/save")
    public ResponseEntity<Rent> carAdd(@Valid @RequestBody Rent car) {
        return ResponseEntity.ok(carRentalService.save(car));
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Message> deleteById( @RequestBody ID id) {
        carRentalService.deleteById(id);
        return ResponseEntity.ok(new Message("Car deleted"));
    }

    @DeleteMapping("/delete")
    public ResponseEntity<Message> delete(@PathVariable Rent rent) {
        carRentalService.delete(rent);
        return ResponseEntity.ok(new Message("Car deleted"));
    }

}
