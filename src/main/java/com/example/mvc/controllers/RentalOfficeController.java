package com.example.mvc.controllers;


import com.example.mvc.entities.RentalOffice;
import com.example.mvc.services.RentalOfficeService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/rentalOffice")
public class RentalOfficeController {


    private final RentalOfficeService rentalOfficeService;

    @GetMapping("/showAll")
    public ResponseEntity<List<RentalOffice>> showAll(@Valid @RequestBody RentalOffice rentalOffice) {
        return ResponseEntity.ok(rentalOfficeService.findAll());
    }

    @GetMapping("/show/{id}")
    public ResponseEntity<RentalOffice> showById(@PathVariable Long id) {
        return ResponseEntity.ok(rentalOfficeService.findById(id));
    }

    @PostMapping("/save")
    public ResponseEntity<RentalOffice> rentalOfficeAdd(@Valid @RequestBody RentalOffice rentalOffice) {
        return ResponseEntity.ok(rentalOfficeService.save(rentalOffice));
    }

}




